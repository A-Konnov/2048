﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrayOutput : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Chip[,] m_chips = gameObject.GetComponent<ArrayCreation>().M_chips;
            for (int i = 0; i < 4; i++)
            {
                Debug.Log(m_chips[i, 0].Number + " " + m_chips[i, 1].Number + " " + m_chips[i, 2].Number + " " + m_chips[i, 3].Number);
            }            
        }
    }
}
