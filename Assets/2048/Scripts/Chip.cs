﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chip
{
    private int number { get; set; }
    public int Number
    {
        get { return number; }
        set { number = value; }
    }
}
