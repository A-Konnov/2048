﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrayCreation : MonoBehaviour
{
    private Chip[,] m_chips = new Chip[4, 4];
    private List<int[]> m_chipsCoord = new List<int[]>();

    public Chip[,] M_chips
    {
        get { return m_chips; }
        set { m_chips = value; }
    }

    void Start()
    {
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                m_chips[i, j] = new Chip();
                m_chips[i, j].Number = 0;
            }
        }

        AddChip(2);
        AddChip(4);
    }

    public void AddChip(int number)
    {
        //собираем пустые ячейки
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                if (m_chips[i, j].Number == 0)
                {
                    m_chipsCoord.Add(new int[2] { i, j });
                }
            }
        }

        //ставим в пустую ячейку фишку
        if (m_chipsCoord.Count > 0)
        {
            int[] thisChipCoord = m_chipsCoord[Random.Range(0, m_chipsCoord.Count)];
            m_chips[thisChipCoord[0], thisChipCoord[1]].Number = number;
        }
        else
        {
            Debug.Log("null");
        }
        m_chipsCoord.Clear();
    }

}
