﻿using UnityEngine;
using System.Collections.Generic;

public class LegBook : ALeg
{
    private string material;
    public override string Material
    {
        get { return material; }

        set { material = value; }
    }
    public override void Stand()
    {
        material = "Book";
        Debug.Log(material + " leg holds a table");

        List<ABook> books = new List<ABook>();
        books.Add(new Book("  Тургенев"));
        books.Add(new Book("  Лермонтов"));
        books.Add(new Book("  Пушкин"));
        books.Add(new Book("  Гоголь"));

        Debug.Log("In a leg of books:");
        foreach (ABook aBook in books)
        {
            aBook.PrintAutor();
        }
    }
}
