﻿using UnityEngine;


public class LegSteel : ALeg
{
    private string material;
    public override string Material
    {
        get { return material; }

        set { material = value; }
    }
    public override void Stand()
    {
        material = "Steel";
        Debug.Log(material + " leg holds a table");
    }
}
