﻿using UnityEngine;


public class LegWood : ALeg
{
    private string material;
    public override string Material
    {
        get { return material; }

        set { material = value; }
    }
    public override void Stand()
    {
        material = "Wood";
        Debug.Log(material + " leg holds a table");
    }
}
