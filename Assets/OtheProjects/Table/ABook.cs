﻿public abstract class ABook
{
    public string Autor { get; set; }
    public ABook (string autor)
    {
        Autor = autor;
    }
    public abstract void PrintAutor();
}
