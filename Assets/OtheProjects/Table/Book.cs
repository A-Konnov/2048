﻿using UnityEngine;

public class Book : ABook
{
    public Book(string autor)
        :base(autor)
    {
        Autor = autor;
    }

    public override void PrintAutor()
    {
        Debug.Log(Autor);
    }
}
