﻿public abstract class ALeg
{
    public abstract string Material { get; set; }
    public abstract void Stand();
}
