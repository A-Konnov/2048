﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Table : MonoBehaviour
{
    public void OnClick()
    {
        List<ALeg> legs = new List<ALeg>();
        legs.Add(new LegWood());
        legs.Add(new LegSteel());
        legs.Add(new LegSteel());
        legs.Add(new LegBook());

        foreach (ALeg aLeg in legs )
        {
            aLeg.Stand();
        }  
    }
}
